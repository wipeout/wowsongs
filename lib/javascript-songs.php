<?php
	function wowsongs_javascript() {
?>
<script>
jQuery(document).ready(function($) {
	//init
	checksongform();

	jQuery("#songform #songform_songtitle, #songform #songform_songartist").blur(function()
	{
		// check of titel/artiest gewijzigd is in songform, en haal remote songdata op
		var title   = jQuery('#songform #songform_songtitle').val();
		var htitle  = jQuery('#songform #songform_hsongtitle').html();
		var artist  = jQuery('#songform #songform_songartist').val();
		var hartist = jQuery('#songform #songform_hsongartist').html();
		if( title != htitle || artist != hartist ) {
			getRemotesong( artist,title );
		}
	});

	jQuery('#songform')
		.on( 'click','$form_cancel',function()
		{
			// klik op de cancelknop - verberg songform en laat songlijst zien
			e.preventDefault();
			jQuery('#songform').hide();
			jQuery('#songs').fadeIn();
		})
		.submit(function(e)
		{
			// Verwerk songdetails
			e.preventDefault();
			var data = jQuery('#songform').serialize();
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				dataType: 'json',
				success: function(response) {
					/**
					 * ajaxcall geeft de songdetails terug - pas de lijst aan
					 */
					jQuery('#songs #song_' + response['songid'] + ' .title' ).html(response['title']);
					jQuery('#songs #song_' + response['songid'] + ' .artistname').html(response['artist']);
					//jQuery('#songs #songform_songbpm').val(response['bpm']);
					if( response['active'] == 1) jQuery('#songs #song_' + response['songid'] + ' .toggleactive').prop('checked', true );
					else jQuery('#song_' + response['songid'] + ' .toggleactive').prop('checked', false);
					if(response['published'] == 1) jQuery('#songs #song_' + response['songid'] + ' .togglepublished').prop('checked', true);
					else jQuery('#song_' + response['songid'] + ' .togglepublished').prop('checked', false);
					/**
					 * form verbergen,  lijst tonen
					 **/
					jQuery('#songform').hide();
					jQuery('#songs').fadeIn();
				}
			});
		});

	jQuery('#songs')
		.on('click','.toggleactive',function(e)
		{
			var id  = jQuery(this).closest('tr').attr('id');
			var data = { 'action':'my_action','do':'toggleactive','songid':id.substring(5) };
			if( jQuery(this).closest('tr').hasClass('inactive') ) {
				jQuery(this).closest('tr').removeClass('inactive');
			} else {
				jQuery(this).closest('tr').addClass('inactive');
			}
			jQuery.post(ajaxurl, data);
		})
		.on('click','.togglepublished',function(e)
		{
			var id  = jQuery(this).closest('tr').attr('id');
			var data = { 'action':'my_action','do':'togglepublished','songid':id.substring(5) };
			if( jQuery(this).closest('tr').hasClass('unpublished') ) {
				jQuery(this).closest('tr').removeClass('unpublished');
			} else {
				jQuery(this).closest('tr').addClass('unpublished');
			}
			jQuery.post(ajaxurl, data);
		})
		.on('click', '.submitedit', function(event)
		{
			// klik op de 'aanpassen' link in de lijst - open het form
			var id  = jQuery(this).closest('tr').attr('id');
			loadSongform(id);
		})
		.on("click","c_submit",function(response)
		{
			var titel = jQuery('#songs #c_songtitel').val();
			var artiest = jQuery('#songs #c_songartiest').val();
			var data = { 'action':'my_action','do':'addsong','titel':titel,'artiest':artiest };
			jQuery.post(ajaxurl, data, function(response) {
				jQuery('#songs tbody').append(response);
				//sorteren
				var sorting = [[1,0],[0,0]];
				jQuery("#songs").trigger("sorton",[sorting])
				//inputveld leegmaken
				jQuery('#songs #c_songtitel').val('');
				jQuery('#songs #c_songartiest').val('');
			});
		})
		.on("click", ".submitdelete", function(response)
		{
			response.preventDefault();
			var id  = jQuery(this).closest('tr').attr('id');
			var data = { 'action':'my_action','do':'deletesong','id':id };
			if( confirm('Zeker weten?') ) {
				jQuery.post(ajaxurl, data, function(response) {
					if( response == '' ) {
						jQuery('#'+ id).fadeOut();
					} else {
						alert(response);
					}
				});
			}
		});

	//check of de toevoegknoppen actief mogen zijn
	jQuery('#songs #c_songtitel').keyup(checksongform);
	jQuery('#songs #c_songartiest').keyup(checksongform);

	//sorteer initieel op artiest,titel
	jQuery("#songs").tablesorter({
		// sort on the second column and first column, order asc
		sortList: [[1,0],[0,0]]
	});

	/* drag-n-drop via jquery */
	var fixHelper = function(e, ui) {
		ui.children().each(function() {
			jQuery(this).width(jQuery(this).width());
		});
		return ui;
	};

	jQuery("#songs tbody").sortable({
		helper: fixHelper
	}).disableSelection();

	function checksongform() {
		var songtitle  = jQuery('#songs #c_songtitel').val();
		var songartist = jQuery('#songs #c_songartiest').val();
		if( typeof(songtitle) == 'undefined' || typeof(songartist) == 'undefined') {

		} else {
			if( songtitle.length > 2 && songartist.length > 2 ) {
				jQuery('#songs #c_submit').removeAttr("disabled");
			} else {
				jQuery('#songs #c_submit').attr("disabled", "disabled");
			}
		}
	}

	function getRemotesong( artist,title ) {
		var data = { 'artist':artist,'title':title };
		jQuery.ajax({
			url: "http://music.wipeout.nu/findsong/",
			data: data,
			dataType:'jsonp',
			contentType: "application/x-www-form-urlencoded;charset=UTF-8",
			error: function(data){
				//alert('mislukt');
			},
			success: function( data ){
				if(data == null)
					return; //geen data, we gaan niet verder
				fillSongform( data );
			}
		})
	}

	function fillSongform( data ) {
//    console.log( ( data ) );
//    if( jQuery('#songform_songtitle').val() == '')
//      jQuery('#songform_songtitle').val(data['title']);
//    if( jQuery('#songform_songartist').val() == '' )
//      jQuery('#songform_songartist').val(data['artist']);
		if( jQuery('#songform_songalbum').val() == '' && data['album'] != '')
			jQuery('#songform_songalbum').val(data['album']);
		if( jQuery('#songform_songwrittenby').val() == '' && data['written'] != '' )
			jQuery('#songform_songwrittenby').val(data['writtenby']);
		if( jQuery('#songform_songlyrics').val() == '' && data['lyrics'] != '' )
			jQuery('#songform_songlyrics').val(data['lyrics'].replace(/(<([^>]+)>)/ig,""));
		if( jQuery('#songform_songyear').val() == '' && data['jaar'] != '' )
			jQuery('#songform_songyear').val(data['jaar']);
	};

	function loadSongform( id ) {
		id = id.substring(5);
		var data = { 'action':'my_action','do':'getsong','songid':id };
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: data,
			dataType: 'json',
			success: function(response) {
				jQuery('#songform_songid').val(id);
				jQuery('#songform_songtitle').val(response['title']);
				jQuery('#songform_hsongtitle').html(response['title']);
				jQuery('#songform_songartist').val(response['artist']);
				jQuery('#songform_hsongartist').html(response['artist']);
				jQuery('#songform_songalbum').val(response['album']);
				jQuery('#songform_songlyrics').val(response['lyrics']);
				jQuery('#songform_songbpm').val(response['bpm']);
				jQuery('#songform_songkey').val(response['songkey']);
				jQuery('#songform_songyear').val(response['songyear']);
				jQuery('#songform_songwrittenby').val(response['writtenby']);
				if(response['active'] == 1) jQuery('#songform_songactive').prop('checked', true);
				if(response['published'] == 1) jQuery('#songform_songpublished').prop('checked', true);
				/**
				* lijst verbergen, form tonen
				**/
				jQuery('#songs').hide();
				jQuery('#songform').fadeIn();

				/**
				* songdetails ophalen van music.wipeout.nl
				**/
				if( (response['title'] != "" && response['artist'] != "") && (response['lyrics'] == "" || response['album'] == "" || response['songyear'] == "") ) {
					getRemotesong( response['artist'],response['title'] );
				}
			}
		});
	}

	/**/

});
</script>
<?php
	}
?>
