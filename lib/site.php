<?php
  /**
   * genereer tabel met songs
   *
   * @return string
   */
  function wow_songs_songTable() {
    /**
     * haal de actieve, gepubliceerde songs op
     */
    $songs = wow_songs_getSongs(true,true);
    $i = 0;
    
    $table = '<div>';
    $table .= '<table id="songtabel">';
    $table .= '<colgroup>';
    $table .= '<col class="wow_songcol" id="titlecol"/>';
    $table .= '<col class="wow_songcol" id="artistcol"/>';
    $table .='</colgroup>';
    $table .= '
      <thead>
        <tr><th>Titel</th><th>Artiest</th></tr>
      </thead>
      <tbody>
    ';
    foreach( $songs as $s ) {
      $table .= '<tr id="song_'.$s->songid.'">';
      $table .= '<td>'.$s->title.'</td>';
      $table .= '<td>'.$s->artist.'</td>';
      $table .= '</tr>';
    }
    $table .= '
      </tbody>
    </table>
    ';
    $table .= '</div>';
    return $table;
  }
  add_shortcode('wowsongtabel', 'wow_songs_songTable');
  
  /**
   * parse contentdata tbv filter
   *
   * @param string $content
   * @return string
   */
//  function wow_songs_parsePage( $content ) {
//    /**
//     * @todo WP shortcode_atts toevoegen (eventueel)
//     * shortcode_atts: http://codex.wordpress.org/Function_Reference/shortcode_atts
//     */
//    $parsedcontent = preg_replace_callback("/\[(WOWSONGS)\]/i","wow_songs_rewriteText",$content);
//    return $parsedcontent;
//  };

  /**
   * geef data terug obv een array van matches
   *
   * @param array $match
   * @return string
   */
//  function wow_songs_rewriteText($match){
//    if(!is_array($match)) return false;
//    $type = $match[1];
//    $var  = $match[2];
//    switch ( strtoupper($type) ) {
//      case "WOWSONGS":
//        return wow_songs_songTable($var);
//        break;
//    }
//  }
  

?>
