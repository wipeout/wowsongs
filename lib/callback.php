<?php

  function wowsongs_callback() {
    $action = $_POST['do'];
    switch ($action) {
      case "addsong":
        $titel   = $_POST['titel'];
        $artiest = $_POST['artiest'];
        $c = wow_songs_addsong( array('title'=>$titel,'artist'=>$artiest) );
        $userdata = get_userdata($c['userid']);
        if($c['active']) $c['achecked'] = " checked=\"checked\"";
        if($c['published']) $c['pchecked'] = " checked=\"checked\"";
      echo '
      <tr id="song_'.$c['songid'].'">
        <td class="title">'.$c['title'].'</td>
        <td class="artist"><span class="artistname">'.$c['artist'].'</span> <span class="row-actions delete"> <a href="#" class="submitdelete" title="'.__("Delete","wowsongs").'"><i class="fa fa-trash-o"></i> </a></span>  <span class="row-actions edit"> <a href="#" class="submitedit" title="'.__("Edit","wowsongs").'"><i class="fa fa-pencil-square-o"></i> </a></span></td>
        <td class="lyrics"><i class="fa fa-microphone-slash"></td>';
			if( TCPDF_VERSION_LOADED ) echo '<td>&nbsp;</td>';
			echo '
        <td class="active centeredtext"><input type="checkbox" name="active" class="toggleactive" '.$c['achecked'].'/></td>
        <td class="published centeredtext"><input type="checkbox" name="published" class="togglepublished" '.$c['pchecked'].'/></td>
        <td class="date">'.date_i18n('j F Y H:i' ,strtotime( $c['createdate'] )).'</td>
        <td class="username">'.$userdata->display_name.'</td>
      </tr>';
        break;
      case "deletesong":
        $id = $_POST['id'];
        list($type,$id) = explode("_",$id);
        if($type == "song") {
          //check of de song in een songdatum voorkomt
          $planned = wow_songs_song_exists( $id );
          if($planned >= 1) {
            //verwijder de song
            wow_songs_deletesong( $id );
          }
        }
        break;
      case "getsong":
        $songid = $_POST['songid'];
        echo json_encode( wow_songs_getsong( $songid ) );
        break;
      case "toggleactive":
        $songid = $_POST['songid'];
        wow_songs_toggleactive( $songid );
        break;
      case "togglepublished":
        $songid = $_POST['songid'];
        wow_songs_togglepublished( $songid );
        break;
      case "editsong":
        $data = $_POST;
        unset($data['action'],$data['do']);
        $ret = wow_songs_updatesong( $data );
        echo json_encode( $ret );
        break;
        
      case "addset":
        $titel   = $_POST['titel'];
        $c = wow_songs_addset( array('title'=>$titel) );
        $userdata = get_userdata($c['userid']);
        if($c['active']) $c['achecked'] = " checked=\"checked\"";
        if($c['published']) $c['pchecked'] = " checked=\"checked\"";
        echo '
        <tr id="set_'.$c['setid'].'" class="setrow'.$trclass.'">
          <td class="title"><span class="settitle">'.$c['title'].'</span> <span class="row-actions delete"> <a href="#" class="submitdelete">'. __("Delete","wowsongs").'</a></span>  <span class="row-actions edit"> <a href="?'.$_SERVER['QUERY_STRING'].'&set='.$c['setid'].'" class="submitedit">'.__("Edit","wowsongs").'</a></span></td>
          <td class="numsongs">'.$c['numsongs'].'</td>
          <td class="numbreaks">'.($c['breaks'] + 1).'</td>
          <td class"date">'.date_i18n('j F Y H:i' ,strtotime( $c['createdate'] )).'</td>
          <td class="username">'.$userdata->display_name.'</td>
        </tr>';        
        break;
      case "deleteset":
        $id = $_POST['id'];
        list($type,$id) = explode("_",$id);
        if($type == "set") {
          wow_songs_deleteset( $id );
        }
        break;
      case "setsetname":
				$setid    = $_POST['setid'];
				$setname  = $_POST['setname'];
				wow_songs_setsetname( $setid,$setname );
				break;
      case "setsetsongs":
        $setid    = $_POST['setid'];
        $setsongs = $_POST['songids'];
        echo "setting setsongs";
        $setsongs = implode(",",$setsongs);
        $setsongs = str_replace("song_","",$setsongs);
        //print_r($setsongs);
        wow_songs_setsetsongs( $setid,$setsongs );
        break;
      case "addsongtoset":
        $setid    = $_POST['setid'];
        $songid   = $_POST['songid'];
        //echo "adding song $songid to set $setid";
        wow_songs_addsong_to_set( $setid,$songid );
        break;
      case "getset":
        $setid = $_POST['setid'];
        echo json_encode( wow_songs_getset( $setid ) );
        break;
//      case "editset":
//        $data = $_POST;
//        unset($data['action'],$data['do']);
//        $ret = wow_songs_updateset( $data );
//        echo json_encode( $ret );
//        break;

    }
    die(); 
  }
