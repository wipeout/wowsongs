<?php
/**
 * html, forms, javascript en ajax-afhandeling voor sets op de adminpagina
 */
function wow_songsplugin_setlist() {
    if (!current_user_can('manage_options'))  {
      wp_die( __("You do not have sufficient permissions to access this page.","wowsongs") );
    }
    $sets = wow_songs_getsets();
    
    /**
     * | settitel [met acties] | aantal songs | aantal sets | gebruikt in gigs | door |
     */
  ?>
    <div class="wrap">
      <h2>Sets</h2>
      <table id="sets" class="wp-list-table widefat fixed posts tablesorter">
        <colgroup>
          <col class="col-title"/>
          <col class="col-numsongs"/>
          <col class="col-numsets"/>
          <col class="col-date"/>
          <col class="col-by"/>
        </colgroup>
        <thead>
          <tr>
            <th><?php _e("Title","wowsongs");?></th>
            <th class="not-sortable"><?php _e("Songs","wowsongs");?></th>
            <th class="not-sortable"><?php _e("Sets","wowsongs");?></th>
            <th class="not-sortable"><?php _e("Date","wowsongs");?></th>
            <th class="not-sortable {sorter: false}"><?php _e("By","wowsongs");?>:</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <td><label><input type="text" name="c_settitel" id="c_settitel"/></label></td>
            <td colspan="4">
            <button type="button" name="c_submit" id="c_submit" class="button-secondary action" disabled="disabled"><?php _e("Add Set","wowsongs"); ?></button>
            </td>
          </tr>
          <tr>
            <th><?php _e("Title","wowsongs");?></th>
            <th class="not-sortable"><?php _e("Songs","wowsongs");?></th>
            <th class="not-sortable"><?php _e("Sets","wowsongs");?></th>
            <th class="not-sortable"><?php _e("Date","wowsongs");?></th>
            <th class="not-sortable {sorter: false}"><?php _e("By","wowsongs");?>:</th>
          </tr>
        </tfoot>
				<tbody>
        <?php
          foreach( $sets as $s ) {
            $trclass  = "";
            
            $userdata = get_userdata($s->userid);
            if( $s->numsongs == "" ) { $s->numsongs = 0; $s->breaks = 0; } else { $s->breaks = $s->breaks + 1; }
            echo '
            <tr id="set_'.$s->setid.'" class="setrow'.$trclass.'">
              <td class="title"><span class="settitle">'.$s->title.'</span> <span class="row-actions delete"> <a href="#" class="submitdelete">'. __("Delete","wowsongs").'</a></span>  <span class="row-actions edit"> <a href="?'.$_SERVER['QUERY_STRING'].'&set='.$s->setid.'" class="submitedit">'.__("Edit","wowsongs").'</a></span></td>
              <td class="numsongs">'.$s->numsongs.'</td>
              <td class="numbreaks">'.$s->breaks.'</td>
              <td class"date">'.date_i18n('j F Y H:i' ,strtotime( $s->createdate )).'</td>
              <td class="username">'.$userdata->display_name.'</td>
            </tr>';
          }
        ?> 
        </tbody>
      </table>
      <?php 
        $setid = $_REQUEST['set'];
        $set   = wow_songs_getset($setid);
      ?>
      
      <form id="setform" action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
        <input type="hidden" id="setform_setid" name="setid" value="<?php echo $set['setid']; ?>"/>
        <input type="hidden" id="setform_action" name="action" value="my_action"/>
        <input type="hidden" id="setform_do" name="do" value="editset"/>
        <table id="setdetails" class="wp-list-table widefat post"> 
          <thead>
          <tr>
            <th>
              <label for="setform_setitle"><?php _e("Title","wowsongs");?></label><br/>
              <label><input type="text" name="title" id="setform_settitle" value="<?php echo $set['title']; ?>"/></label>
            </th>
          </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
          <tr>
            <th colspan="9">
              <button type="button" id="form_submit" class="button-secondary action"> <?php _e("OK","wowsongs");?></button>
              <button type="button" id="form_cancel" class="button-secondary action"><?php _e("Cancel","wowsongs");?></button>
            </th>
          </tr>
          </tfoot>
        </table>
      </form>
      
      <?php 
        if( isset( $setid ) ) {
          $songs        = wow_songs_getsongs();
          $insetsongs   = wow_songs_getsongs( "set_{$setid}" );
          $setsongids   = $set['setlist'];
          $setsongids   = explode(",",$setsongids);
        
      ?>
      <h2><?php _e("In this setlist","wowsongs");?></h2>
      <table id="insetsongs" class="wp-list-table widefat fixed posts tablesorter connectedSortable">
        <colgroup>
          <col class="col-title"/>
          <col class="col-artist"/>
          <col class="col-bpm"/>
        </colgroup>
        <thead>
          <tr>
            <th><?php _e("Title","wowsongs");?></th>
            <th><?php _e("Artist","wowsongs");?></th>
            <th><?php _e("BPM","wowsongs");?></th>
            <th class="not-sortable"><?php _e("Date","wowsongs");?></th>
            <th class="not-sortable {sorter: false}"><?php _e("By","wowsongs");?>:</th>
          </tr>
        </thead>
        <tfoot>
        </tfoot>
        <tbody>
        <?php
        if( ( count($insetsongs) > 0 ) ) {
          foreach($insetsongs as $s) {
            if($s->songid == "" ) continue;
            $trclass  = "";
//            $achecked = "";
//						$inset = "";
            
            $userdata = get_userdata($s->userid);
            if( in_array($s->songid,$setsongids ) )
						{
							$trclass .= " inset";
//							$achecked = "checked = \"checked\"";
//							$inset = "&gt;";
						}
            
            echo '
            <tr id="song_'.$s->songid.'" class="setsongrow'.$trclass.'" title="'.$s->songid.'">
              <td class="title">'.$s->title.'</td>
              <td class="artist"><span class="artistname">'.$s->artist.'</span></td>
              <td class="bpm">'.$s->bpm.'</td>
              <td class"date">'.date_i18n('j F Y H:i' ,strtotime( $s->createdate )).'</td>
              <td class="username">'.$userdata->display_name.'</td>
            </tr>';
          }
        }
      ?> 
        </tbody>
      </table><br/>

			<h2><?php _e("Available songs","wowsongs");?></h2>
      <table id="setsongs" class="wp-list-table widefat fixed posts tablesorter connectedSortable">
        <colgroup>
          <col class="col-title"/>
          <col class="col-artist"/>
          <col class="col-bpm"/>
        </colgroup>
        <thead>
          <tr>
            <th><?php _e("Title","wowsongs");?></th>
            <th><?php _e("Artist","wowsongs");?></th>
            <th><?php _e("BPM","wowsongs");?></th>
            <th class="not-sortable"><?php _e("Date","wowsongs");?></th>
            <th class="not-sortable {sorter: false}"><?php _e("By","wowsongs");?>:</th>
          </tr>
        </thead>
        <tfoot>
        </tfoot>
        <tbody>
        <?php
        if( isset( $setid ) ) {

          foreach($songs as $s) {
            if( in_array($s->songid,$setsongids) ) continue;
            $trclass  = "";
//            $achecked = "";
//            $inset = "";

            $userdata = get_userdata($s->userid);
            if( in_array($s->songid,$setsongids ) )
						{
							$trclass  .= " inset";
//							$achecked  = "checked = \"checked\"";
//							$inset     = "&gt;";
						}
            
            echo '
            <tr id="song_'.$s->songid.'" class="setsongrow'.$trclass.'" title="'.$s->songid.'">
              <td class="title">'.$s->title.'</td>
              <td class="artist"><span class="artistname">'.$s->artist.'</span></td>
              <td class="bpm">'.$s->bpm.'</td>
              <td class"date">'.date_i18n('j F Y H:i' ,strtotime( $s->createdate )).'</td>
              <td class="username">'.$userdata->display_name.'</td>
            </tr>';
          }
        }
      ?> 
        </tbody>
      </table>
<?php } ?>
    </div> <!-- /wrap -->
 <?php }


