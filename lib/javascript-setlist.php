<?php
	function wowsongs_sets_javascript() {
?>
<script>
jQuery(document).ready(function($) {
	//init
	checksetform();
	// sorteer initieel op titel
	jQuery("#setsongs").tablesorter({
		//sort on the second column and first column, order asc
		sortList: [[1,0]]
	});

	<?php
		if( isset($_REQUEST['set']) && $_REQUEST['set'] > 0 ) {
	?>
		jQuery('#setform, #setsongs').show();
		jQuery('#sets').hide();
	<?php
		}
	?>

	// klik op de 'aanpassen' link in de lijst - open het setform
	jQuery('#sets').on('click','submitedit', function(e)
	{
		var id  = jQuery(this).closest('tr').attr('id');
		loadSongform(id);
	});

	jQuery('#setform')
	.on('click', '#form_cancel',function(e)
	{
		// klik op de cancelknop - verberg setform en laat setslijst zien
		e.preventDefault();
		history.go(-1);
		jQuery('#setform,#insetsongs,#setsongs').hide();
		jQuery('#sets').fadeIn();
	})
	.submit(function(e)
	{
		e.preventDefault();
		var data = jQuery('#songform').serialize();
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: data,
			dataType: 'json',
			success: function(response) {
				/**
				* ajaxcall geeft de songdetails terug - pas de lijst aan
				*/
				jQuery('#songs #song_' + response['songid'] + ' .title' ).html(response['title']);
				jQuery('#songs #song_' + response['songid'] + ' .artistname').html(response['artist']);
				//jQuery('#songs #songform_songbpm').val(response['bpm']);
				if( response['active'] == 1)
					jQuery('#songs #song_' + response['songid'] + ' .toggleactive').prop('checked', true );
				else
					jQuery('#song_' + response['songid'] + ' .toggleactive').prop('checked', false);
				if(response['published'] == 1)
					jQuery('#songs #song_' + response['songid'] + ' .togglepublished').prop('checked', true);
				else
					jQuery('#song_' + response['songid'] + ' .togglepublished').prop('checked', false);
				/**
				* form verbergen,  lijst tonen
				**/
				jQuery('#songform').hide();
				jQuery('#songs').fadeIn();
			}
		});
	});

//  klik op checkbox toggle active - voeg song toe aan deze set
//  jQuery('#setform #setsongs .toggleinset').click(function(e) {
//    var setid  = jQuery('#songform_setid').val();
//    var songid = jQuery(this).closest('tr').attr('id').substring(5);
//    var data   = { 'action':'my_action','do':'addsongtoset','songid':id,'setid': setid };
//    if( jQuery(this).closest('tr').hasClass('inset') ) {
//      jQuery(this).closest('tr').removeClass('inset');
//    } else {
//      jQuery(this).closest('tr').addClass('inset');
//    }
//    jQuery.post(ajaxurl, data);
//  });

	//check of de toevoegknoppen actief mogen zijn
	jQuery('#sets #c_settitel').keyup(checksetform);

	jQuery('#setdetails')
		.on("click","#form_submit",function(response)
		{
			response.preventDefault();
			var setname = jQuery("#setform_settitle").val();
			var setid   = jQuery("#setform_setid").val();
			jQuery.post(ajaxurl,{ 'action':'my_action','do':'setsetname', 'setname':setname, 'setid':setid },function(response){

			})
		});

	jQuery('#sets')
	.on('click','#c_submit',function(response)
	{
		//set toevoegen
		var titel = jQuery('#c_settitel').val();
		var data = { 'action':'my_action','do':'addset','titel':titel };
		jQuery.post(ajaxurl, data, function(response) {
			//nieuwe set toevoegen aan de lijst
			jQuery('#sets tbody').append(response);
			//sets sorteren op titel
			var sorting = [[0,0]];
			jQuery("#sets").trigger("sorton",[sorting])
			//inputveld leegmaken
			jQuery('#sets #c_songtitel').val('');
			jQuery('#sets #c_songartiest').val('');
		});
	})
	.on('click','.submitdelete',function(response)
		{
		//set verwijderen
		response.preventDefault();
		var id  = jQuery(this).closest('tr').attr('id');
		var data = { 'action':'my_action','do':'deleteset','id':id };
		if( confirm('<?php _e('Are you sure you want to delete this set?','wowsongs'); ?>') ) {
			jQuery.post(ajaxurl, data, function(response) {
				if( response == '' ) {
					jQuery('#'+ id).fadeOut();
				} else {
					alert(response);
				}
			});
		}
	});

	/* drag-n-drop via jquery ui */
	var fixHelper = function(e, ui) {
		ui.children().each(function() {
			jQuery(this).width(jQuery(this).width());
		});
		return ui;
	};

	jQuery("#insetsongs tbody, #setsongs tbody").sortable({
		helper: fixHelper,
		connectWith: ".connectedSortable tbody",
		opacity: 0.6,
		revert: true,
		cursor: 'move',
//		handle: '.hndle',
		update: function(){
			console.log('sortable1');
		}
	}).disableSelection();

	//klik op beschikbare songs om deze naar de set te verplaatsen
	jQuery("#setsongs tbody").on("click","tr",function(e)
	{
		var row = jQuery(this);
		var setid = jQuery('#setform_setid').val();
		var songid = row.attr('id').substring(5);
		var data = { 'action':'my_action', 'do':'addsongtoset','setid':setid, 'songid':songid };

		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: data,
			success: function(response) {
//        jQuery("#insetsongs tbody").append(jQuery(this));
				jQuery("#insetsongs tbody").append(row);
			}
		});

		jQuery("#insetsongs tbody").append(jQuery(this));
	});

	//bij hersorteren even de nieuwe lijst opslaan...
	$( "#insetsongs tbody" ).bind( "sortupdate", function(event, ui) {
		var setid = jQuery('#setform_setid').val();
		var ids = $("#insetsongs tbody").sortable('toArray');
		var data   = { 'action':'my_action','do':'setsetsongs','songids':ids,'setid': setid };
		jQuery.post(ajaxurl, data);
	});

	//klik op song in set om deze uit de set te verwijderen
	jQuery("#insetsongs tbody tr").live("click",function(e){
		jQuery("#setsongs tbody").append(jQuery(this));
		var setid = jQuery('#setform_setid').val();
		var ids = $("#insetsongs tbody").sortable('toArray');
		var data   = { 'action':'my_action','do':'setsetsongs','songids':ids,'setid': setid };
		jQuery.post(ajaxurl, data);
		//sorteer songlijst
		var sorting = [[1,0]];
		jQuery("#setsongs").trigger("sorton",[sorting])
//    console.log(ids);
	})

	function checksetform() {
		var settitle = jQuery('#sets #c_settitel').val();
		if( typeof(settitle) == 'undefined' ) {

		} else {
			if( settitle.length > 4 ) {
				jQuery('#sets #c_submit').removeAttr("disabled");
			} else {
				jQuery('#sets #c_submit').attr("disabled", "disabled");
			}
		}
	}

});
</script>
<?php  }
