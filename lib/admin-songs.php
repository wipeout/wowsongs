<?php
/**
 * html, lijst en forms voor adminpagina songs
 */

	function wow_songsplugin_options() {
		if (!current_user_can('manage_options'))  {
			wp_die( __("You do not have sufficient permissions to access this page.","wowsongs") );
		}
		$songs = wow_songs_getsongs("admin");

	?>
		<div class="wrap">
			<h2>Songs</h2>
			<table id="songs" class="wp-list-table widefat fixed posts tablesorter">
				<colgroup>
					<col class="col-title"/>
					<col class="col-artist"/>
					<col class="col-lyrics"/>
					<col class="col-pdf"/>
					<col class="col-active"/>
					<col class="col-published"/>
					<col class="col-date"/>
					<col class="col-user"/>
				</colgroup>
				<thead>
					<tr>
						<th><?php _e("Title","wowsongs");?></th>
						<th><?php _e("Artist","wowsongs");?></th>
						<th class="sorter-false"><i class="fa fa-microphone" title="<?php _e("Lyrics","wowsongs");?>"></i></th>
						<?php if( TCPDF_VERSION_LOADED ) { ?> <th class="sorter-false"><i class="fa fa-file-text-o"></i></th><?php } ?>
						<th class="sorter-false"><?php _e("Active","wowsongs");?></th>
						<th class="sorter-false"><?php _e("Show","wowsongs");?></th>
						<th class="sorter-false"><?php _e("Date","wowsongs");?></th>
						<th class="sorter-false"><?php _e("By","wowsongs");?>:</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th><?php _e("Title","wowsongs");?></th>
						<th><?php _e("Artist","wowsongs");?></th>
						<th class="sorter-false"><i class="fa fa-microphone" title="<?php _e("Lyrics","wowsongs");?>"></i></th>
						<?php if( TCPDF_VERSION_LOADED ) { ?> <th class="sorter-false"><i class="fa fa-file-text-o"></i></th><?php } ?>
						<th class="sorter-false"><?php _e("Active","wowsongs");?></th>
						<th class="sorter-false"><?php _e("Show","wowsongs");?></th>
						<th class="sorter-false"><?php _e("Date","wowsongs");?></th>
						<th class="sorter-false"><?php _e("By","wowsongs");?>:</th>
					</tr>
					<tr>
						<td><input type="text" name="c_songtitel" id="c_songtitel" placeholder="<?php _e("Title","wowsongs");?>"/></td>
						<td><input type="text" name="c_songartiest" id="c_songartiest" placeholder="<?php _e("Artist","wowsongs");?>"/></td>
						<td colspan="4"><button type="button" name="c_submit" id="c_submit" class="button-secondary action" disabled="disabled"><?php _e("Add Song","wowsongs"); ?></button></td>
					</tr>
				</tfoot>
				<tbody>
				<?php
					foreach($songs as $s) {
						$trclass  = "";
						$achecked = "";
						$pchecked = "";

						$userdata = get_userdata($s->userid);
						if( !$s->published ) { $trclass  = " unpublished"; } else { $pchecked  = "checked = \"checked\""; }
						if( !$s->active )    { $trclass .= " inactive"; } else { $achecked = "checked = \"checked\"";}
						if( strlen($s->lyrics) > 0 ) { $faclass = ""; } else { $faclass = "-slash";}
						echo '
						<tr id="song_'.$s->songid.'" class="songrow'.$trclass.'">
							<td class="title">'.$s->title.'</td>
							<td class="artist"><span class="artistname">'.$s->artist.'</span> <span class="row-actions delete"> <a href="#" class="submitdelete" title="'. __("Delete","wowsongs").'"><i class="fa fa-trash-o"></i> </a></span>  <span class="row-actions edit"> <a href="#" class="submitedit" title="'.__("Edit","wowsongs").'"><i class="fa fa-pencil-square-o"></i> </a></span></td>
							<td class="lyrics"><i class="fa fa-microphone'.$faclass.'"></td>';
						if( TCPDF_VERSION_LOADED )
						{
							if( strlen($s->lyrics) )
								echo '<td><i class="fa fa-file-text-o"></i></td>';
							else
								echo '<td>&nbsp;</td>';
						}
						echo '
							<td class="active centeredtext"></i><input type="checkbox" name="active" class="toggleactive" '.$achecked.'/></td>
							<td class="published centeredtext"><input type="checkbox" name="published" class="togglepublished" '.$pchecked.'/></td>
							<td class"date">'.date_i18n('j F Y H:i' ,strtotime( $s->createdate )).'</td>
							<td class="username">'.$userdata->display_name.'</td>
						</tr>';
						//unset($trclass,$pcheck,$achecked);
					}
				?>
				</tbody>
			</table>

			<form id="songform" action="<?php echo $_SERVER['PHP_SELF']?>" method="post">

				<input type="hidden" id="songform_songid" name="songid"/>
				<input type="hidden" id="songform_action" name="action" value="my_action"/>
				<input type="hidden" id="songform_do" name="do" value="editsong"/>
				<table id="pop" class="wp-list-table widefat post">
					<thead>
					<tr>
						<th>
							<label for="songform_songtitle"><?php _e("Title","wowsongs");?></label><br/>
							<input type="text" name="title" id="songform_songtitle"/>
						</th>
						<th>
							<label for="songform_songartist"><?php _e("Artist","wowsongs");?></label><br/>
							<input type="text" name="artist" id="songform_songartist"/>
						</th>
						<th>
							<label for="songform_songwrittenby"><?php _e("Writer","wowsongs");?></label><br/>
							<input type="text" name="writtenby" id="songform_songwrittenby"/>
						</th>
						<th>
							<label for="songform_songalbum"><?php _e("Album","wowsongs");?></label><br/>
							<input type="text" name="album" id="songform_songalbum"/>
						</th>
					</tr>
					<tr>
						<td>
							<label for="songform_songyear"><?php _e("Year","wowsongs");?></label><br>
							<input type="text" name="songyear" id="songform_songyear" maxlength="4"/>
						</td>
						<td>
							<label for="songform_songbpm" class="label-bpm"><?php _e("BPM","wowsongs");?></label>
							<label for="songform_songkey"><?php _e("Key","wowsongs");?></label><br/>
							<input type="text" name="bpm" id="songform_songbpm"/>
							<input type="text" name="songkey" id="songform_songkey"/>
						</td>
						<td>
							<label class="label-songactive" for="songform_songactive"><?php _e("Active","wowsongs");?></label>
							<input type="checkbox" name="active" id="songform_songactive"/><br/>
							<label class="label-songpublished" for="songform_songpublished"><?php _e("Show","wowsongs");?></label>
							<input type="checkbox" name="published" id="songform_songpublished"/>
						</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="9" class="centered">
							<label for="songform_songlyrics"><?php _e("Lyrics","wowsongs");?></label><br>
							<textarea name="lyrics" id="songform_songlyrics"></textarea>
							<span id="songform_hsongtitle"></span><span id="songform_hsongartist"></span>
						</td>
					</tr>
					</tbody>
					<tfoot>
					<tr>
						<th colspan="9">
							<input type="submit" id="form_submit" class="button-secondary action" value="<?php _e("OK","wowsongs");?>" id="post_songdetails">
							<button id="form_cancel" class="button-secondary action"><?php _e("Cancel","wowsongs");?></button>
						</th>
					</tr>
					</tfoot>
				</table>
			</form>
		</div> <!-- /wrap -->
		<?php if( TCPDF_VERSION_LOADED ) echo "TCPDF plugin loaded: v" . TCPDF_VERSION_LOADED; ?>
 <?php }
