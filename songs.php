<?php
/*
Plugin Name: WipeOut! songlist plugin
Plugin URI: http://www.wipeout.nu/wordpress
Description: Plugin, ontwikkeld voor bands. Te gebruiken voor songmanagement. gebruik shortcode [wowsongtabel] om een tabel met songs te tonen.
Version: 1.0
Author: WipeOut! webservices
Author URI: http://www.wipeout.nu/
License: GPL2
*/
/*  Copyright 2013  Dennis Lutz  (email : dennis@wipeout.nu)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
/**
 * @todo add button to editor
 * @todo setsongs in twee tabellen, eerste met setsongs, tweede met overige nummers.
 *       klik op song in tweede, of slepen zet nummer in setsongs, slepen in setsongs bepaalt de volgorde
 * @todo remove deleted-marked songs when they're not in any setlist
 * @todo (for future gig-plugin) remove deleted-marked sets when they're not in any gig
 * @todo implement pdf (ncpdf?) to download setlist/lyrics
 */
/**
 * @var wpdb $wpdb
 */

	require_once(plugin_dir_path( __FILE__ ).'lib/admin-songs.php');
	require_once(plugin_dir_path( __FILE__ ).'lib/admin-setlist.php');
	require_once(plugin_dir_path( __FILE__ ).'lib/javascript-songs.php');
	require_once(plugin_dir_path( __FILE__ ).'lib/javascript-setlist.php');
	require_once(plugin_dir_path( __FILE__ ).'lib/callback.php');
	require_once(plugin_dir_path( __FILE__ ).'lib/site.php');

	define("SONGTABLE",$wpdb->prefix . "wow_songs");
	define("SETSTABLE",$wpdb->prefix . "wow_setlist");

	wp_register_style('wow_songs', plugin_dir_url( __FILE__ ) . 'style/wow_songs_site.css');
	wp_register_script( 'tablesorter', plugin_dir_url( __FILE__ ) .'js/jquery.tablesorter.2.0.min.js');
	//wp_register_style('jquery.ui.theme','//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css');
	wp_register_style('font-awesome','//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css');
	wp_register_style('wow_songs_admin', plugin_dir_url( __FILE__ ) . 'style/wow_songs_admin.css');

/**
 * adminmenuoptie toevoegen aan admin menu
 * voeg javascript, stylesheet en callback toe
 */
	add_action('admin_menu', 'wow_songsplugin_menu');
	add_action('admin_footer', 'wowsongs_javascript');
	add_action('wp_ajax_my_action', 'wowsongs_callback');
	//wp_enqueue_script('jquery-ui-datepicker');
	//wp_enqueue_script('jquery-ui-dialog');
	//wp_enqueue_script('jquery-ui-sortable');
	wp_enqueue_style('wow_songs');

	/**
	 * voeg contentfilter toe om de tabel (en eventueel andere content) in te voegen
	 */
	//add_filter('the_content','wow_songs_parsePage');

	add_action('init', 'wow_songsplugin_lang_init');

	/**
	 * initieer database
	 */
	register_activation_hook(__FILE__,'wow_songs_install');

	/**
	 * Voeg het menu en submenu's toe
	 *
	 */
	function wow_songsplugin_menu() {
		/**
		 * voeg een menuoptie toe aan admin menu
		 * voeg javascript en callbackscripts toe voor admin
		 * voeg stylesheets toe voor admin
		 */
		add_action('admin_footer', 'wowsongs_javascript');
		add_action('admin_footer', 'wowsongs_sets_javascript');
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_script( 'jquery-ui-sortable' );
		wp_enqueue_script( 'tablesorter' );
		wp_enqueue_style( 'wow_songs_admin' );
		wp_enqueue_style( 'font-awesome' );
		//wp_enqueue_style('jquery.ui.theme');

		/**
		 * Admin: hoofdmenu
		 */
		$page_title = 'WipeOut! songs';
		$menu_title = 'Songs';
		$capability = 'edit_pages';
		$menu_slug  = 'wow_songsplugin';
		$function   = 'wow_songsplugin_options';
		$icon_url   = plugin_dir_url( __FILE__ ).'icon_music.png';
		// Als er een andere plugin dezelfde positie heeft gekozen kunnen deze elkaar overlappen/schrijven
		//$position   = 3;
		$position   = null;
		add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );

		/**
		 * Admin: submenu's songmanagement
		 */

		//add_submenu_page($menu_slug,'','',$capability,$menu_slug,$function);
		$parent_slug = $menu_slug;
		$page_title  = 'Set management';
		$menu_title  = 'Setlists';
		$capability  = 'edit_pages';
		$menu_slug   = 'setsmanagement';
		$function    = "wow_songsplugin_setlist";
		add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );

		$parent_slug = $menu_slug;
		$page_title  = 'Songs management';
		$menu_title  = 'Songs management';
		$capability  = 'edit_pages';
		$menu_slug   = 'songsmanagement';
		$function    = "songsmanagement";
		add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
	}

	/**
	 * localisation bestanden laden
	 *
	 */
	function wow_songsplugin_lang_init() {
		load_plugin_textdomain( 'wowsongs', false, dirname( plugin_basename( __FILE__ ) ).'/lang' );
	}

	/**
	 * Database setup; maak de tabellen aan
	 */
	global $wow_songs_db_version;
	$wow_songs_db_version = "1.0";

/**
 * Installeer de databasecomponenten
 */
function wow_songs_install()
{
		global $wpdb;
		global $wow_songs_db_version;
		//$wpdb->show_errors();

		$table_name1  = $wpdb->prefix . "wow_songs";

		$sql = "
			CREATE TABLE $table_name1 (
				songid MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
				title VARCHAR(55) NOT NULL,
				artist VARCHAR(55) NOT NULL,
				writtenby VARCHAR(55) NOT NULL,
				songkey VARCHAR(25) NOT NULL,
				bpm VARCHAR(25) NOT NULL,
				album VARCHAR(55) NOT NULL,
				songyear YEAR(4) NOT NULL,
				lyrics TEXT DEFAULT '',
				active TINYINT(1) DEFAULT 1,
				published TINYINT(1) DEFAULT 1,
				createdate DATETIME NOT NULL,
				updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				userid MEDIUMINT(9) NOT NULL,
				deleted TINYINT(1) DEFAULT 0,
				PRIMARY KEY (songid),
				UNIQUE KEY titleartist (title,artist),
				KEY title (title),
				KEY artist (artist)
			);
		";

		$table_name  = $wpdb->prefix . "wow_setlist";
		$sql2 = "
			CREATE TABLE $table_name (
				setid MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
				title VARCHAR(55) NOT NULL,
				setlist TEXT NOT NULL,
				createdate DATETIME NOT NULL,
				updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				userid MEDIUMINT(9) NOT NULL,
				deleted TINYINT(1) DEFAULT 0,
				PRIMARY KEY (setid),
				KEY title (title)
			);
		";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);

	 /**
	 * Voeg de 'break' toe. Altijd met id 1 (belangrijk!
	 * @var wpdb $wpdb
	 */

	$data = array( 'title'=>' - break', 'artist'=>'', 'writtenby'=>'', 'songkey'=>'', 'bpm'=>'', 'album'=>'', 'songyear'=>'0000', 'createdate'=>'0000-00-00 00:00:00', 'userid'=>1 );
		$result = $wpdb->insert( $table_name1, $data );

		dbDelta($sql2);

		add_option("wow_songs_db_version", $wow_songs_db_version);
	}

	/**
	 * voeg een song toe aan database en return de songgegevens
	 *
	 * @var wpdb $wpdb
	 * @param array $data
	 * @return array
	 */
function wow_songs_addsong( $data ) {
		error_log(__FUNCTION__."(".print_r($data,true).")");
		if( !is_array( $data ) || $data['title'] == "" ) { print_r($data); return false; }
		global $wpdb;
		$current_user = wp_get_current_user();
		$data['userid'] = $current_user->ID;
		$data['createdate'] = "0000-00-00 00:00:00";

		$result = $wpdb->insert( SONGTABLE, $data );
		wow_songs_syncdate();
		return wow_songs_getsong( $wpdb->insert_id );
	}

	/**
	 * Update een song
	 *
	 * @var wpdb $wpdb
	 * @param array $data
	 * @return array
	 */
	function wow_songs_updatesong( $data ) {
		if( !is_array( $data ) || $data['title'] == "" ) { print_r($data); return false; }
		global $wpdb;
//    $wpdb->show_errors();
		$current_user = wp_get_current_user();
		$data['userid'] = $current_user->ID;
		if( $data['active'] == "on" ) $data['active'] = 1;
		if( $data['published'] == "on" ) $data['published'] = 1;
		$data['lyrics'] = stripslashes( $data['lyrics']);
		$ret = $wpdb->update( SONGTABLE, $data,array("songid"=>$data['songid']) );
		return wow_songs_getsong( $data['songid'] );
	}

/**
 * Mark a song Deleted
	 * @param $songid
	 * @param bool $forced set to true to delete from database
	 * @return bool
	 */
	function wow_songs_deletesong( $songid, $forced=false )
	{
		global $wpdb;
		//$wpdb->show_errors();
		if(is_numeric($songid))
			$w = "songid={$songid}";
		else
			return false;
		if( $forced )
			$wpdb->query("DELETE FROM ".SONGTABLE." WHERE {$w}");
		else
			$wpdb->query("UPDATE ".SONGTABLE." SET deleted=1 WHERE {$w}");
	};

	function wow_songs_toggleactive( $songid )
	{
		global $wpdb;
		if( !is_numeric($songid) )
			return false;
		$sql = "SELECT active FROM ".SONGTABLE." WHERE songid = {$songid}";
		$isactive = $wpdb->get_var( $wpdb->prepare( $sql ) );
		if( $isactive == 1)
			$wpdb->query("UPDATE ".SONGTABLE." SET active=0 WHERE songid={$songid}");
		else
			$wpdb->query("UPDATE ".SONGTABLE." SET active=1 WHERE songid={$songid}");
	};

	/**
	 * toggle publicatiestatus van een song
	 *
	 * @var wpdb $wpdb
	 * @param integer $songid
	 * @return bool
	 */
	function wow_songs_togglepublished( $songid )
	{
		global $wpdb;
		if( !is_numeric($songid) )
			return false;
		$sql = "SELECT published FROM ".SONGTABLE." WHERE songid = {$songid}";
		$isactive = $wpdb->get_var( $wpdb->prepare( $sql ) );
		if( $isactive == 1)
			$wpdb->query("UPDATE ".SONGTABLE." SET published=0 WHERE songid={$songid}");
		else
			$wpdb->query("UPDATE ".SONGTABLE." SET published=1 WHERE songid={$songid}");
	};

	/**
	 * haal gegevens van een song op
	 *
	 * @param mixed $songid songnaam of songid
	 * @return array
	 */
	function wow_songs_getsong( $songid )
	{
		global $wpdb;
		if( !is_numeric( $songid ) )
			return false;
		$sql = "SELECT * FROM ".SONGTABLE." WHERE songid={$songid}";
		$res = $wpdb->get_row( $sql, ARRAY_A );
		return $res;
	};

	/**
	 * sync empty creation date with updated date
	 *
	 */
	function wow_songs_syncdate()
	{
		global $wpdb;
		$wpdb->query("UPDATE ".SONGTABLE." SET createdate=updated WHERE createdate='0000-00-00 00:00:00'");
	};

	/**
	 * check of een bepaalde song al voorkomt in de database
	 *
	 * @param string/integer $title songtitle or songid
	 * @param string $artist optional
	 * @return integer 0 or 1
	 */
	function wow_songs_song_exists( $title,$artist="" )
	{
		global $wpdb;
		//$wpdb->show_errors();
		if( is_numeric( $title ) )
		{
			$sql = "SELECT COUNT(*) FROM ".SONGTABLE." WHERE songid = {$title}";
		}
		else
		{
			$title  = strtolower($title);
			$artist = strtolower($artist);
			$sql = "SELECT COUNT(*) FROM ".SONGTABLE." WHERE LOWER(title) = '{$title}' AND LOWER(artist)='{$artist}'";
		}
		$res = $wpdb->get_var( $wpdb->prepare( $sql ) );
		return $res;
	};

	/**
	 * Haal alle songs op
	 *
	 * @param integer/boolean $active "all" of "admin" laat alles zien, set_1 laat songs uit set zien
	 * @param integer/boolean $published
	 * @return array
	 */
	function wow_songs_getsongs( $active=1,$published=1 )
	{
		global $wpdb;
		if( $active === "admin" || substr($active,0,3) == "all" )
		{
			$sql = "SELECT * FROM ".SONGTABLE." WHERE deleted=0 AND songid>1 ORDER BY artist,title";
		}
		elseif( substr($active,0,4) == "set_" )
		{
			list(,$setid) = explode("_",$active);
			$sql = "SELECT setlist FROM ".SETSTABLE." WHERE setid = {$setid}";
			$songids = $wpdb->get_var( $wpdb->prepare( $sql ) );
			/**
			 * onderstaande gaat niet werken met meerdere gelijke nummers in de IN()
			 */
			//$sql = "SELECT * FROM ".SONGTABLE." WHERE songid IN({$songids}) ORDER BY FIND_IN_SET(songid,'{$songids}')";
			$songids = explode(",",$songids);
			foreach( $songids as $id )
			{
				$ret[] = $wpdb->get_row( "SELECT * FROM ".SONGTABLE." WHERE songid ={$id}" );
			}
			return (object)$ret;
		}
		else
		{
			if( $active ) $active = 1; else $active = 0;
			if( $published ) $published = 1; else $published = 0;
			$sql = "SELECT * FROM ".SONGTABLE." WHERE active={$active} AND published={$published} AND deleted=0 AND songid>1 ORDER BY artist,title";
		}

		$songs = $wpdb->get_results( $sql );
		return $songs;
	};

	function wow_songs_getsets()
	{
		global $wpdb;
//    $sql = "
//      SELECT
//        *,
//        IF( setlist = '',0,
//          SUM(
//            LENGTH(REPLACE(setlist, ',-1', '')) - LENGTH(
//              REPLACE(REPLACE(setlist, ',-1', ''), ',', '')
//            ) + 1
//          )
//        ) AS numsongs,
//        ROUND( (SUM(LENGTH(setlist) - LENGTH(REPLACE(setlist, '-1', ''))) /2),0)  AS breaks
//      FROM ".SETSTABLE."
//      GROUP BY setid
//      ORDER BY title
//    ";
		$sql = "SELECT * FROM ".SETSTABLE." GROUP BY setid ORDER BY title";

		$songs = $wpdb->get_results( $sql );

	 /**
		* bereken aantal songs en aantal breaks in deze set
		*/
		foreach( $songs as $s )
		{
			$breaks = 0;
			$numsongs = 0;
			if( strlen($s->setlist) > 0 ) {
				$ss = explode(",",$s->setlist);
				$numitems = count($ss);
				if( $numitems > 0) {
					foreach( $ss as $ff ) { if($ff == 1) $breaks++; else continue; }
				}
				$numsongs = ($numitems - $breaks);
			}
			$s->numsongs = $numsongs;
			$s->breaks   = $breaks;
			$ret[] = $s;
		}
		return (object)$ret;
		return $songs;
	};

	function wow_songs_getset( $setid )
	{
		global $wpdb;
		if( !is_numeric( $setid ) ) {
			return false;
		}
		$sql = "SELECT * FROM ".SETSTABLE." WHERE setid={$setid}";
		$res = $wpdb->get_row( $sql, ARRAY_A );
		return $res;
	};

	function wow_songs_addsong_to_set( $setid,$songid )
	{
		global $wpdb;
		$set = wow_songs_getset( $setid );
		$songids = array();
		if( strlen($set['setlist']) ) $songids = explode(",",$set['setlist']);
		$songids[] = $songid;
		$songids = implode(",",$songids);
		$songids = trim($songids,",");
		$wpdb->query("UPDATE ".SETSTABLE." SET setlist='{$songids}' WHERE setid={$setid}");
	};

	function wow_songs_removesong_from_set( $setid,$songid )
	{
		global $wpdb;
		$set = wow_songs_getset( $setid );
		$songids = explode(",",$set['setlist']);
		$songids[] = $songid;
		$songids = implode(",",$songids);
		$songids = trim($songids,",");
		$wpdb->query("UPDATE ".SETSTABLE." SET setlist='{$songids}' WHERE setid={$setid}");
	};

	function wow_songs_setsetname( $setid, $setname )
	{
		global $wpdb;
		$wpdb->query("UPDATE ".SETSTABLE." SET title='{$setname}' WHERE setid={$setid}");
	}

	function wow_songs_setsetsongs( $setid,$songids )
	{
		global $wpdb;
		if( is_array($songids) ) $songids = implode(",",$songids);
		$songids = trim($songids,",");
		$wpdb->query("UPDATE ".SETSTABLE." SET setlist='{$songids}' WHERE setid={$setid}");
	};

	function wow_songs_addset( $data )
	{
		if( !is_array( $data ) || $data['title'] == "" ) { print_r($data); return false; }
		global $wpdb;
		$current_user = wp_get_current_user();
		$data['userid'] = $current_user->ID;
		$data['createdate'] = "0000-00-00 00:00:00";

		$result   = $wpdb->insert( SETSTABLE, $data );
		$newsetid = $wpdb->insert_id;
		wow_songs_syncsetdate();

		return wow_songs_getset( $newsetid );
	};

	function wow_songs_deleteset( $setid )
	{
		global $wpdb;
		if( !is_numeric($setid) ) {
			return false;
		}
		//$wpdb->query("UPDATE ".SETSTABLE." set deleted=1 WHERE setid={$setid}");
		$wpdb->query("DELETE FROM ".SETSTABLE." WHERE setid={$setid}");
	};

	function wow_songs_syncsetdate()
	{
		global $wpdb;
		$wpdb->query("UPDATE ".SETSTABLE." SET createdate=updated WHERE createdate='0000-00-00 00:00:00'");
	};

