=== Plugin Name ===
Contributors: dennislutz
Donate link: http://wipeout.nu/
Tags: band, songs, songmanagement, setlist
Requires at least: 2.0.2
Tested up to: 3.8
Stable tag: 1.0
License: GPLv2

Plugin, ontwikkeld voor bands. Te gebruiken voor songmanagement. gebruik shortcode [wowsongtabel] om een tabel met songs te tonen.

== Description ==

Plugin, ontwikkeld voor bands. Te gebruiken voor songmanagement. gebruik shortcode `[wowsongtabel]` om een tabel met songs te tonen.

== Installation ==

1. Upload all files to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the directory of the stable readme.txt, so in this case, `/tags/4.3/screenshot-1.png` (or jpg, jpeg, gif)

== Changelog ==

= 1.0 =
* Initial version

= 0.5 =
* Database only ;-)

== Upgrade Notice ==

= 1.0 =
Please upgrade if you want to actually see something

